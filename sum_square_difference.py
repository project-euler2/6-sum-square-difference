def sum_square_difference(upper_limit):
    square_sum = 0
    for i in range(upper_limit + 1):
        square_sum += i**2 
    sum_squared = (sum(range(upper_limit + 1))) ** 2
    return sum_squared - square_sum

print(sum_square_difference(100))